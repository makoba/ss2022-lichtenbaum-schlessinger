# ss2022-lichtenbaum-schlessinger

These are notes for a talk I am giving in the PhD-seminar (organized by Ludvig Modin and Anneloes Viergever).
A compiled version can be found [here](https://makoba.gitlab.io/ss2022-lichtenbaum-schlessinger/lichtenbaum-schlessinger.pdf).