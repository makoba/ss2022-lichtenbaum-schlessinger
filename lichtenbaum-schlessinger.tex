\documentclass[10pt, a4paper]{scrartcl}

\usepackage{packages}
\usepackage{commands}
\bibliography{references.bib}

\title{The Lichtenbaum-Schlessinger complex and the (upper) cotangent functors}
\author{Manuel Hoff}
\date{}

\begin{document}

    \maketitle

    These are notes for a talk I am giving in the PhD-seminar on \emph{Deformation Theory} taking place in the summer term 2022 in Essen.
    The main reference for this talk is \cite{lichtenbaum-schlessinger}.

    I thank the organizers of the seminar, Ludvig Modin and Anneloes Viergever, for helping me with the preparation.

    \section{Outline}

    Given a map of rings $A \to B$ one can associate to it the module $\Omega_{B/A} \in \catmods(B)$ of Kähler differentials.
    It comes with an $A$-linear derivation $d \colon B \to \Omega_{B/A}$ and satisfies a certain universal property.

    The association $(A \to B) \mapsto \Omega_{B/A}$ is functorial in the sense that a commutative square
    \[
    \begin{tikzcd}
        A \arrow{r} \arrow{d}
        & A' \arrow{d}
        \\
        B \arrow{r}
        & B'
    \end{tikzcd}
    \]
    of rings induces a map $\Omega_{B/A} \to \Omega_{B'/A'}$ in $\catmods(B)$, or equivalently a map $B' \otimes_{B} \Omega_{B/A} \to \Omega_{B'/A'}$ in $\catmods(B')$.
    Now given ring maps $A \to B \to C$ the induced sequence
    \[
        C \otimes_B \Omega_{B/A} \to \Omega_{C/A} \to \Omega_{C/B} \to 0
    \]
    is exact.
    In general however the first map in the sequence is not injective.
    One might thus wonder if it is possible to somehow \enquote{derive} the functor of Kähler differentials in order to obtain a continuation of the exact sequence to the left.
    Of course the classical theory of derived functors does not apply here as the category of rings is far from being Abelian.

    However, there is a way to make sense of derived functors in this setting and this yields the \emph{cotangent complex} $L_{B/A}$ of a ring map $A \to B$ (due to André-Quillen).
    This is a (homological) chain complex of $B$-modules, well defined up to quasi-isomorphism and concentrated in degrees $\geq 0$ with $H_0(L_{B/A}) \cong \Omega_{B/A}$.
    It \enquote{contains a lot of information} about the deformation theory of $B$ as an $A$-algebra.

    We will not construct the cotangent complex $L_{B/A}$ as its definition is a little involved and uses the machinery of homotopical algebra.
    Instead the goal of this talk is to introduce the Lichtenbaum-Schlessinger complex $\LS_{B/A}$ whose construction is relatively explicit.
    It is a chain complex of $B$-modules concentrated in $[0, 2]$ and well-defined up to homotopy equivalence, and it turns out that it identifies with the truncation of the cotangent complex.
    
    \section{Notation}

    \begin{itemize}
        \item
        $\catrings$ is the category of (commutative and unital) rings.
        
        \item
        $\catmods$ is the category of pairs $(A, M)$ consisting of a ring $A$ and an $A$-module $M$.
        For $A \in \catrings$ we denote by $\catmods(A)$ the category of $A$-modules, i.e.\ the fiber of $\catmods \to \catrings$ over $A$.

        \item
        Similarly we also set $\catalgs$, $\catch_{\bullet}$ and $\cathom_{\bullet}$ to be the category of algebras, the category of (homological) chain complexes and the homotopy category of chain complexes respectively.
        As the category $\catmods$, all these categories have a projection to $\catrings$.

        For $A \in \catrings$ we then have the category $\catalgs(A)$ of $A$-algebras, the category $\catch_{\bullet}(A)$ of chain complexes of $A$-modules and its homotopy category $\cathom_{\bullet}(A)$.

        \item
        Note that $\catalgs$ can be described as the functor category $\Fun([1], \catrings)$ (where $[1]$ is the totally ordered set $\curlybr{0 < 1}$).
        It thus comes with two evaluation maps $\ev_0, \ev_1 \colon \catalgs \to \catrings$.
    \end{itemize}

    \section{\texorpdfstring{Construction of $\LS_{B/A}$}{Construction of the Lichtenbaum-Schlessinger complex}}

    Our aim is now to construct the Lichtenbaum-Schlessinger complex $\LS_{B/A}$ of a map of rings $A \to B$.
    More precisely we will construct a functor $\LS \colon \catalgs \to \cathom_{\bullet}$ making the triangle
    \[
    \begin{tikzcd}
        \catalgs \arrow{rr}{\LS} \arrow{rd}[swap]{\ev_1}
        && \cathom_{\bullet} \arrow{ld}
        \\
        & \catrings
    \end{tikzcd}
    \]
    commutative.
    In order to do this we first define a functor $\LStild \colon \catalgs^+ \to \catch_{\bullet}$ (where $\catalgs^+$ is the category of ring maps $A \to B$ together with some extra data) and then show that it induces a functor $\LS$ as desired.

    \subsection{\texorpdfstring{The category $\catalgs^+$}{The category Alg+}}

    \begin{definition}[{\cite[Definitions 2.1.1 and 2.1.3]{lichtenbaum-schlessinger}, \cite[Construction 3.1]{hartshorne}}] \label{def:algplus}
        We define the category $\catalgs^+$ as follows.
        \begin{itemize}
            \item
            Its objects $\calE$ consist of $A \in \catrings$ and an exact sequence
            \[
                0 \to E_2 \xrightarrow{e_2} E_1 \xrightarrow{e_1} R \xrightarrow{e_0} B \to 0
            \]
            given as follows.
            \begin{itemize}
                \item
                $B, R \in \catalgs(A)$ and $e_0 \colon R \to B$ is a map in $\catalgs(A)$.

                \item
                $E_1, E_2 \in \catmods(R)$ and $e_1, e_2$ are maps in $\catmods(R)$.
            \end{itemize}
            These data are required to satisfy the following two \enquote{freeness properties}:
            \begin{itemize}
                \item
                $R$ is a polynomial $A$-algebra, i.e.\ it is isomorphic to $A[(t_i)_{i \in \Gamma}]$ for some index set $\Gamma$.

                \item
                There exists a short exact sequence
                \[
                    0 \to U \xrightarrow{i} F \xrightarrow{j} I \to 0
                \]
                in $\catmods(R)$, where $I = \ker(e_0) \subseteq R$ and $F$ is free, such that
                \[
                    (E_2 \xrightarrow{e_2} E_1 \xrightarrow{e_1} R) \cong (U/U_0 \xrightarrow{i} F/U_0 \xrightarrow{j} R)
                \]
                in $\Fun([2], \catmods(R))$ (with the isomorphism $R \to R$ being the identity) where $U_0 \subseteq U$ is the image of the map $F \otimes_R F \to F$ given by $x \otimes y \mapsto j(x)y - j(y)x$.
            \end{itemize}
            Note that the last point in particular implies that we have $e_1(x)y = e_1(y)x$ for all $x, y \in E_1$.

            \item
            Maps $\alpha \colon \calE \to \calE'$ in $\catalgs^+$ consist of a map $a \colon A \to A'$ in $\catrings$ and a commutative diagram
            \[
            \begin{tikzcd}
                0 \arrow{r}
                & E_2 \arrow{r} \arrow{d}{\alpha_2}
                & E_1 \arrow{r} \arrow{d}{\alpha_1}
                & R \arrow{r} \arrow{d}{\alpha_0}
                & B \arrow{r} \arrow{d}{b}
                & 0
                \\
                0 \arrow{r}
                & E'_2 \arrow{r}
                & E'_1 \arrow{r}
                & R' \arrow{r}
                & B' \arrow{r}
                & 0
            \end{tikzcd}
            \]
            with $b, \alpha_0$ maps in $\catalgs(A)$ and $\alpha_1, \alpha_2$ maps in $\catmods(R)$.
        \end{itemize}
        We have a natural forgetful functor $\catalgs^+ \to \catalgs$ given by $\calE \mapsto (A \to B)$.
        Objects of $\catalgs^+$ are called \enquote{free (two-term) extensions} in \cite{lichtenbaum-schlessinger} but we prefer not to use this terminology.
    \end{definition}

    \begin{lemma}[{\cite[text before Definition 2.1.3]{lichtenbaum-schlessinger}}] \label{lem:algplus-1}
        The functor $\catalgs^+ \to \catalgs$ is surjective on objects.
    \end{lemma}

    \begin{proof}
        Suppose we are given $(A \to B) \in \catalgs$.
        Then we can define $R \coloneqq A[(t_{\lambda})_{\lambda \in B}] \in \catalgs(A)$ and
        \[
            e_0 \colon R \to B, \qquad t_{\lambda} \mapsto \lambda.
        \]
        Setting $I \coloneqq \ker(e_0)$ we can define $F \coloneqq \bigoplus_{\lambda \in I} R \cdot e_{\lambda}$ and
        \[
            j \colon F \to I, \qquad e_{\lambda} \mapsto \lambda.
        \]
        Now we can set $U \coloneqq \ker(j)$ and define $U_0$ as in \Cref{def:algplus}.
        Finally we can define $E_1 \coloneqq F/U_0$ and $E_2 \coloneqq U/U_0$.
        The maps $e_1$ and $e_2$ are then induced by $j$ and the inclusion $U \subseteq F$ respectively.
        Altogether we have obtained an object $\calE \in \catalgs^+$ that maps to $(A \to B)$.
    \end{proof}

    \begin{lemma}[{\cite[Proposition 2.1.7]{lichtenbaum-schlessinger}}] \label{lem:algplus-2}
        For $\calE, \calE' \in \catalgs^+$ the map
        \[
            \Hom_{\catalgs^+}(\calE, \calE') \to \Hom_{\catalgs}((A \to B), (A' \to B'))
        \]
        is surjective.
    \end{lemma}

    \begin{proof}
        Suppose we are given $(a, b) \colon (A \to B) \to (A' \to B')$.
        Then we construct a preimage $\alpha \colon \calE \to \calE'$ as follows.
        \begin{itemize}
            \item
            As $R$ is a polynomial $A$-algebra and $e'_0 \colon R' \to B'$ is surjective we can lift $R \xrightarrow{e_0} B \xrightarrow{b} B'$ to a map of $A$-algebras $\alpha_0 \colon R \to R'$ making the triangle
            \[
            \begin{tikzcd}
                & R' \arrow{d}{e'_0}
                \\
                R \arrow{ru}{\alpha_0} \arrow{r}[swap]{b e_0}
                & B'
            \end{tikzcd}
            \]
            commutative.

            \item
            Choose an exact sequence
            \[
                0 \to U \to F \xrightarrow{j} I \to 0
            \]
            and an isomorphism $(E_0 \to E_1 \to R) \cong (U/U_0 \to F/U_0 \to R)$ as in \Cref{def:algplus}.
            As $F$ is a free $R$-module and the map $e'_1 \colon E'_1 \to I'$ is surjective we can lift $F \xrightarrow{j} I \xrightarrow{\alpha_0} I'$ to a map of $R$-modules $\widetilde{\alpha}_1 \colon F \to E'_1$ making the triangle
            \[
            \begin{tikzcd}
                & E'_1 \arrow{d}{e'_1}
                \\
                F \arrow{ru}{\widetilde{\alpha}_1} \arrow{r}[swap]{\alpha_0 j}
                & I'
            \end{tikzcd}
            \]
            commutative.

            \item
            Now for $x, y \in F$ we have
            \begin{align*}
                \widetilde{\alpha}_1(j(x)y - j(y)x)
                &= (\alpha_0 j)(x) \widetilde{\alpha}_1(y) - (\alpha_0 j)(y) \widetilde{\alpha}_1(x) \\
                &= e'_1(\widetilde{\alpha}_1(x)) \widetilde{\alpha}_1(y) - e'_1(\widetilde{\alpha}_1(y)) \widetilde{\alpha}_1(x) = 0.
            \end{align*}
            Thus $\widetilde{\alpha}_1(U_0) = 0$ so that we obtain an induced map $\alpha_1 \colon E_1 \cong F/U_0 \to E'_1$ in $\catmods(R)$.

            \item
            $\alpha_2 \colon E_2 \to E'_2$ is now simply induced by $\alpha_1$ and $\alpha_0$.
        \end{itemize}
        Now $\alpha = (a, b, \alpha_0, \alpha_1, \alpha_2) \colon \calE \to \calE'$ is the desired preimage.
    \end{proof}

    \begin{notation}
        Given two maps $\alpha, \beta \colon \calE \to \calE'$ in $\catalgs^+$ we write $\alpha \sim \beta$ if they map to the same map in $\catalgs$.
    \end{notation}

    \begin{corollary} \label{cor:factor-functor}
        The functor $\catalgs^+ \to \catalgs$ induces an equivalence of categories $(\catalgs^+/\sim) \to \catalgs$ (that is surjective on objects).
    \end{corollary}

    \begin{proof}
        This follows from \Cref{lem:algplus-1}, \Cref{lem:algplus-2} and formal nonsense.
    \end{proof}

    \subsection{\texorpdfstring{Defining $\LStild \colon \catalgs^+ \to \catch_{\bullet}$}{Defining LStilde}}
    
    \begin{lemma}[{\cite[Definition 2.1.1]{lichtenbaum-schlessinger}}]
        Let $\calE \in \catalgs^+$.
        Then $I E_2 = 0$ so that $E_2$ naturally is a $B$-module.
    \end{lemma}

    \begin{proof}
        Let $\lambda \in I$ and $x \in E_2$.
        Then we can choose $y \in E_1$ with $e_1(y) = \lambda$ and calculate
        \[
            e_2(\lambda x) = \lambda e_2(x) = e_1(y) e_2(x) = e_1 e_2(x) y = 0
        \]
        so that $\lambda x = 0$ as $e_2$ is injective.
    \end{proof}

    \begin{definition}[{\cite[Definition 2.1.2]{lichtenbaum-schlessinger}}]
        Let $\calE \in \catalgs^+$.
        Then we define $\LStild(\calE) \in \catch_{\bullet}(B)$ as the complex
        \[
            0 \to E_2 \xrightarrow{d_2} B \otimes_R E_1 \xrightarrow{d_1} B \otimes_R \Omega_{R/A} \to 0
        \]
        where the last term sits in degree zero and where the differentials are given as follows.
        \begin{itemize}
            \item
            $d_2$ is induced by $e_2$.

            \item
            $d_1$ is given by the composition $B \otimes_R E_1 \to B \otimes_R I \cong I/I^2 \to B \otimes_R \Omega_{R/A}$ where the first map is induced by $e_1$ and the second map is the natural map
            \[
                I/I^2 \to B \otimes_R \Omega_{R/A}, \qquad \lambda \mapsto 1 \otimes d \lambda.
            \]
        \end{itemize}
        This construction is clearly functorial, i.e.\ gives rise to a functor $\LStild \colon \catalgs^+ \to \catch_{\bullet}$ making the triangle
        \[
        \begin{tikzcd}
            \catalgs^+ \arrow{rr}{\LStild} \arrow{rd}
            && \catch_{\bullet} \arrow{ld}
            \\
            & \catrings
        \end{tikzcd}
        \]
        commutative (where the left vertical map sends $\calE$ to $B$).

        Note that the terms in degree $0$ and $1$ of the complex $\LStild(\calE)$ are in fact free $B$-modules.
        In degree $0$ this follows because $R$ is a polynomial algebra and in degree $1$ it follows because $B \otimes_R E_1 \cong B \otimes_R F$ for $F$ as in \Cref{def:algplus}.
    \end{definition}

    \subsection{\texorpdfstring{Showing that $\LStild$ induces a functor $\LS \colon \catalgs \to \cathom_{\bullet}$}{Showing that LStilde induces a functor LS}}

    \begin{definition}[{\cite[Definition 2.1.4]{lichtenbaum-schlessinger}}]
        Let $(A \to R) \in \catalgs$.
        Let $M \in \catmods(R \otimes_A R)$ and denote the two induced $R$-module structures on $M$ by $\cdot$ and $\star$ (they are given by restriction of scalars along the two structure maps $R \to R \otimes_A R$).

        An \emph{$A$-linear $R$-biderivation (with values in $M$)} is an $A$-linear map $\varepsilon \colon R \to M$ such that
        \[
            \varepsilon(\lambda \mu) = \lambda \cdot \varepsilon(\mu) + \mu \star \varepsilon(\lambda)
        \]
        for all $\lambda, \mu \in R$.
    \end{definition}

    \begin{lemma}[{\cite[Lemma 2.1.6]{lichtenbaum-schlessinger}}] \label{lem:lifting}
        Let $A \in \catrings$.
        Let $R, S \in \catalgs(A)$ and let $f, g \colon R \to S$ be maps in $\catalgs(A)$.
        \begin{itemize}
            \item
            Consider $S$ as an $(R \otimes_A R)$-algebra via the map $(f, g)$ (in particular $S$ now has the structure of an $(R \otimes_A R)$-module).
            Then the map $f - g \colon R \to S$ is an $A$-linear $R$-biderivation.

            \item
            Let $M \in \catmods(S)$ and let $u \colon M \to S$ be a map in $\catmods(S)$ such that $u(x) y = u(y) x$ for all $x, y \in M$.
            Suppose furthermore that $\im(f - g) \subseteq \im(u)$ and that $R$ is a polynomial $A$-algebra.
            Then there exists an $A$-linear $R$-biderivation $\varepsilon \colon R \to M$ such that $u \varepsilon = f - g$.
        \end{itemize}
    \end{lemma}

    \begin{proof}
        For the first part we just calculate
        \[
            (f - g)(\lambda \mu) = f(\lambda \mu) - g(\lambda \mu) = f(\lambda) f(\mu) - g(\lambda) g(\mu) = f(\lambda) \cdot (f - g)(\mu) + g(\mu) \cdot (f - g)(\lambda).
        \]
        For the second part let us fix an isomorphism $R = A[(t_i)_{i \in \Gamma}]$ for some index set $\Gamma$.
        By assumption there exist elements $x_i \in M$ such that $u(x_i) = f(t_i) - g(t_i)$.
        Define $\varepsilon \colon R \to M$ by setting
        \[
            \varepsilon(t_{i_1} \dotsm t_{i_k}) \coloneqq \sum_{j = 1}^k f(t_{i_1} \dotsm t_{i_{j - 1}}) g(t_{i_{j + 1}} \dotsm t_{i_k}) x_{i_j}
        \]
        for $i_1, \dotsc, i_k \in I$ and extending by $A$-linearity.
        Let us check that this is well defined, i.e.\ independent under permutation of the indices.
        To do this it clearly suffices to only consider a transposition of the form $(p, p+1)$ where the difference between the defining expressions is given by
        \begin{align*}
            &f(t_{i_1} \dotsm t_{i_{p - 1}}) g(t_{i_p} t_{i_{p + 2}} \dotsm t_{i_k}) x_{i_{p + 1}}
            \\
            + &f(t_{i_1} \dotsm t_{i_{p - 1}} t_{i_{p + 1}}) g(t_{i_{p + 2}} \dotsm t_{i_k}) x_{i_p} \\
            - &f(t_{i_1} \dotsm t_{i_{p - 1}}) g(t_{i_{p + 1}} \dotsm t_{i_k}) x_{i_p} \\
            - &f(t_{i_1} \dotsm t_{i_p}) g(t_{i_{p + 2}} \dotsm t_{i_k}) x_{i_{p + 1}} \\
            = f(t_{i_1} \dotsm t_{i_{p - 1}}) &g(t_{i_{p + 2}} \dotsm t_{i_k}) \cdot \roundbr[\Big]{u(x_{i_{p + 1}}) x_{i_p} - u(x_{i_p}) x_{i_{p + 1}}} = 0.
        \end{align*}
        Now to check that $\varepsilon$ is an $R$-biderivation we calculate
        \begin{align*}
            \varepsilon((t_{i_1} \dotsm t_{i_k}) \cdot &(t_{i_{k + 1}} \dotsm t_{i_{k + l}})) \\
            &= \sum_{j = 1}^{k + l} f(t_{i_1} \dotsm t_{i_{j - 1}}) g(t_{i_{j + 1}} \dotsm t_{i_{k + l}}) x_{i_j} \\
            &= f(t_{i_1} \dotsm t_{i_k}) \cdot \sum_{j = 1}^l f(t_{i_{k + 1}} \dotsm t_{i_{k + j - 1}}) g(t_{i_{k + j + 1}} \dotsm t_{i_{k + l}}) x_{i_{k + j}} \\ 
            & \qquad + g(t_{i_{k + 1}} \dotsm t_{i_{k + l}}) \cdot \sum_{j = 1}^k f(t_{i_1} \dotsm t_{i_{j - 1}}) g(t_{i_{j + 1}} \dotsm t_{i_k}) x_{i_j} \\
            &= f(t_{i_1} \dotsm t_{i_k}) \varepsilon(t_{i_{k + 1}} \dotsm t_{i_{k + l}}) + g(t_{i_{k + 1}} \dotsm t_{i_{k + l}}) \varepsilon(t_{i_1} \dotsm t_{i_k}).
        \end{align*}
        By $A$-linearity of $\varepsilon$ it then follows that $\varepsilon(\lambda \mu) = f(\lambda) \varepsilon(\mu) + g(\mu) \varepsilon(\lambda)$ for all $\lambda, \mu \in R$ as desired.

        Now it is left to check the identity $u \varepsilon = f - g$.
        But it suffices to check this identity on the generators $t_i$ where we have $u \varepsilon(t_i) = u(x_i) = f(t_i) - g(t_i)$.
    \end{proof}

    \begin{lemma}[{\cite[Lemma 2.1.5]{lichtenbaum-schlessinger}}] \label{prop:homotopic}
        Let $\calE, \calE' \in \catalgs^+$ and let $\alpha = (a, b, \alpha_0, \alpha_1, \alpha_2) \sim \beta = (a, b, \beta_0, \beta_1, \beta_2)$ be two maps $\calE \to \calE'$ in $\catalgs^+$.
        Then the two maps $\LStild(\alpha), \LStild(\beta) \colon \LStild(\calE) \to \LStild(\calE')$ in $\catch_{\bullet}$ are homotopic.
    \end{lemma}

    \begin{proof}
        Our goal is to construct a homotopy $(H_0, H_1)$ between $\LStild(\alpha), \LStild(\beta)$.
        First we construct $H_0$:
        \begin{itemize}
            \item
            Applying \Cref{lem:lifting} to the two $A$-algebra maps $\alpha_0, \beta_0 \colon R \to R'$ and the map $e'_1 \colon E'_1 \to R'$ of $R'$-modules we obtain an $A$-linear $R$-biderivation $\varepsilon \colon R \to E'_1$ such that $e'_1 \varepsilon = \alpha_0 - \beta_0$.

            \item
            Postcomposing $\varepsilon$ with the projection $E'_1 \to B' \otimes_{R'} E'_1 = \LStild(\calE')_1$ we obtain an $A$-linear $R$-derivation $\overline{\varepsilon} \colon R \to \LStild(\calE')_1$ (note that $e'_0 \alpha_0 = e'_0 \beta_0 = b e_0$).

            \item
            By the universal property of the Kähler differentials $\overline{\varepsilon}$ induces an $R$-linear map $\Omega_{R/A} \to \LStild(\calE')_1$.
            Extending scalars gives a $B$-linear map $H_0 \colon \LStild(\calE)_0 = B \otimes_R \Omega_{R/A} \to \LStild(\calE')_1$.
        \end{itemize}
        The identity $e'_1 \varepsilon = \alpha_0 - \beta_0$ implies that $d'_1 H_0 = \LStild(\alpha)_0 - \LStild(\beta)_0$.

        Now we construct $H_1$:
        \begin{itemize}
            \item
            Define $\theta \coloneqq \alpha_1 - \beta_1 - \varepsilon e_1 \colon E_1 \to E'_1$.
            This is (a priori) an $A$-linear map.

            \item
            Note that
            \[
                e'_1 \theta = e'_1 (\alpha_1 - \beta_1 - \varepsilon e_1) = (\alpha_0 - \beta_0 - e'_1 \varepsilon) e_1 = 0
            \]
            so that $\theta$ induces a map $E_1 \to E'_2$ that we again denote by $\theta$ by abuse of notation.

            \item
            We claim that $\theta$ is actually $R$-linear (note that the two $R$-module structures on $E'_1$ agree when restricted to $E'_2$).
            To do this we calculate for $\lambda \in R$ and $x \in E_1$
            \[
                \theta(\lambda x) = \alpha_0(\lambda) \alpha_1(x) - \beta_0(\lambda) \beta_1(x) - \alpha_0(\lambda) \varepsilon(e_1(x)) - \beta_0(e_1(x)) \varepsilon(\lambda)
            \]
            and
            \[
                \alpha_0(\lambda) \theta(x) = \alpha_0(\lambda) \alpha_1(x) - \alpha_0(\lambda) \beta_1(x) - \alpha_0(\lambda) \varepsilon(e_1(x)).
            \]
            The difference of these two expressions is given by
            \[
                (\alpha_0(\lambda) - \beta_0(\lambda)) \beta_1(x) - \beta_0(e_1(x)) \varepsilon(\lambda) = e'_1(\varepsilon(\lambda)) \beta_1(x) - e'_1(\beta_1(x)) \varepsilon(\lambda) = 0
            \]
            so that $\theta$ is indeed $R$-linear.

            \item
            Now $\theta$ induces a $B$-linear map $H_1 \colon \LStild(\calE)_1 = B \otimes_R E_1 \to E'_2 = \LStild(\calE')_2$.
        \end{itemize}
        Now the identity $\varepsilon e_1 + \theta = \alpha_1 - \beta_1$ implies that $H_0 d_1 + d'_2 H_1 = \LStild(\alpha)_1 - \LStild(\beta)_1$.
        Moreover we have $\theta e_2 = \alpha_1 e_2 - \beta_1 e_2$ so that we obtain $H_1 d_2 = \LStild(\alpha)_2 - \LStild(\beta)_2$.

        Thus we have constructed the desired homotopy.
    \end{proof}

    Now \Cref{prop:homotopic} implies that there exists a unique functor $(\catalgs^+/\sim) \to \cathom_{\bullet}$ making the diagram
    \[
    \begin{tikzcd}
        \catalgs^+ \arrow{r}{\LStild} \arrow{d}
        & \catch_{\bullet} \arrow{d}
        \\
        (\catalgs^+/\sim) \arrow[dashed]{r}{\exists !}
        & \cathom_{\bullet}
    \end{tikzcd}
    \]
    commutative.
    Thus we can give the following definition.

    \begin{definition}[{\cite[Definition 2.1.8]{lichtenbaum-schlessinger}}]
        Choose a section $s \colon \catalgs \to (\catalgs^+/\sim)$ (such a section exists by \Cref{cor:factor-functor}).
        Then define
        \[
            \LS \colon \catalgs \to \cathom_{\bullet}, \qquad (A \to B) \mapsto \LS_{B/A}
        \]
        as the composition $\catalgs \xrightarrow{s} (\catalgs^+/\sim) \to \cathom_{\bullet}$.
        We call $\LS_{B/A} \in \cathom_{\bullet}(B)$ the \emph{Lichtenbaum-Schlessinger complex} of the ring map $A \to B$.

        Given $\calE \in \catalgs^+$ with image $(A \to B) \in \catalgs$ we always have a canonical isomorphism $\LS_{B/A} \cong \LStild(\calE)$ in $\cathom_{\bullet}(B)$.
    \end{definition}

    \begin{definition}[{\cite[Sections 2.3 and 2.3.6]{lichtenbaum-schlessinger}, \cite[Construction 3.1 and Theorem 3.4]{hartshorne}}]
        For $(A \to B) \in \catalgs$ we define the \emph{(upper) cotangent functors}
        \[
            T^i(B/A, \blank) \coloneqq H^i(\Hom_B(\LS_{B/A}, \blank)) \colon \catmods(B) \to \catmods(B)
        \]
        for $i = 0, 1, 2$.
        $(T^i(B/A, \blank))_i$ naturally has the structure of a truncated cohomological $\delta$-functor.
    \end{definition}

    \begin{remark}
        In \cite{lichtenbaum-schlessinger} the authors introduce the lower cotangent functors $T_i(B/A, \blank) \colon \catmods(B) \to \catmods(B)$ that are given by $T_i(B/A, M) \coloneqq H_i(\LS_{B/A} \otimes_B M)$.
        However, we will not use these in our seminar.
    \end{remark}

    \section{\texorpdfstring{Properties of $\LS_{B/A}$}{Properties of the Lichtenbaum-Schlessinger complex}}

    \begin{theorem}[{\cite[Theorem 2.2.4 and Section 2.3.5]{lichtenbaum-schlessinger}, \cite[Theorem 3.5]{hartshorne}}] \label{lem:transitivity}
        Suppose we are given maps of rings $A \to B \to C$ and let $\calE, \calG \in \catalgs^+$ projecting to $(A \to B)$ and $(B \to C)$.
        Then there exists $\calF \in \catalgs^+$ projecting to $(A \to C)$ and maps $\calE \to \calF \to \calG$ projecting to $(A \to B) \to (A \to C) \to (B \to C)$ such that the sequence
        \[
            0 \to C \otimes_B \LStild(\calE) \to \LStild(\calF) \to \LStild(\calG) \to 0
        \]
        in $\catch_{\bullet}(C)$ is exact in degrees $0, 1$ and right exact in degree $2$.
        In particular we obtain a (not so) long exact sequence
        \begin{align*}
            0 \to &T^0(C/B, M) \to T^0(C/A, M) \to T^0(B/A, M) \to \\
            \to &T^1(C/B, M) \to T^1(C/A, M) \to T^1(B/A, M) \to \\
            \to &T^2(C/B, M) \to T^2(C/A, M) \to T^2(B/A, M)
        \end{align*}
        for every $M \in \catmods(C)$.
    \end{theorem}

    \begin{proof}
        Write $\calE = (A, 0 \to U/U_0 \to F/U_0 \to R \to B \to 0)$ and $\calG = (B, 0 \to W/W_0 \to H/W_0 \to T \to C \to 0)$ as in \Cref{def:algplus}, choose isomorphisms $R \cong A[(t_i)_{i \in \Gamma}]$ in $\catalgs(A)$ and $T \cong B[(u_j)_{j \in \Delta}]$ in $\catalgs(B)$ and set $I = \ker(R \to B)$ and $K = \ker(T \to C)$.

        Now define $S = A[(t_i)_{i \in \Gamma}, (u_j)_{j \in \Delta}]$ and set $J = \ker(S \to C)$.
        Choose a free $S$-module $H^*$ together with an isomorphism $T \otimes_S H^* \cong H$ in $\catmods(T)$ and set $G = (S \otimes_R F) \oplus H^*$.
        Choose $u \colon G \to J$ fitting into the commutative diagram
        \[
        \begin{tikzcd}
            0 \arrow{r}
            & S \otimes_R F \arrow{r} \arrow{d}
            & G \arrow{r}{j} \arrow{d}{u}
            & H^* \arrow{r} \arrow{d}
            & 0
            \\
            0 \arrow{r}
            & S \otimes_R I \arrow{r}
            & J \arrow{r}
            & K \arrow{r}
            & 0
        \end{tikzcd}
        \]
        in $\catmods(S)$ with exact rows and where all the vertical maps are surjective.
        Put $V = \ker(u \colon G \to J)$ and $W^* = \ker(H^* \to K)$ so that we obtain a short exact sequence
        \[
            0 \to S \otimes_R U \to V \to W^* \to 0
        \]
        (where we use that $S$ is flat over $R$).

        At this point we have constructed $\calF = (A, 0 \to V/V_0 \to G/V_0 \to S \to C \to 0) \in \catalgs^+$ and maps $\calE \to \calF \to \calG$ as in the statement of the theorem.
        We are now left with showing that the diagram
        \[
        \begin{tikzcd}
            & C \otimes_R U/U_0 \arrow{r} \arrow{d}
            & V/V_0 \arrow{r} \arrow{d}
            & W/W_0 \arrow{r} \arrow{d}
            & 0
            \\
            0 \arrow{r}
            & C \otimes_R F \arrow{r} \arrow{d}
            & C \otimes_S G \arrow{r} \arrow{d}
            & H \otimes_T C \arrow{r} \arrow{d}
            & 0
            \\
            0 \arrow{r}
            & C \otimes_R \Omega_{R/A} \arrow{r}
            & C \otimes_S \Omega_{S/A} \arrow{r}
            & C \otimes_T \Omega_{T/B} \arrow{r}
            & 0
        \end{tikzcd}
        \]
        has exact rows (the exactness of the $9$-term exact sequence of cotangent functors then follows formally).
        \begin{itemize}
            \item
            The lower row identifies with the sequence
            \[
                0 \to \bigoplus_{i \in \Gamma} C \cdot dt_i \to \bigoplus_{i \in \Gamma} C \cdot dt_i \oplus \bigoplus_{j \in \Delta} C \cdot du_j \to \bigoplus_{j \in \Delta} C \cdot du_j \to 0
            \]
            that clearly is exact.

            \item
            The middle row is obtained from the exact sequence
            \[
                0 \to S \otimes_R F \to G \to H^* \to 0
            \]
            by applying $C \otimes_S \blank$ and thus is exact.

            \item
            The upper row is a complex because the composition $F \to G \to H$ vanishes and the map $V/V_0 \to W/W_0$ is surjective because $V \to W^*$ is surjective.

            \item
            Finally let us show that the top row is exact in the middle.
            So suppose that we are given an element $x \in V$ mapping to $0$ in $W/W_0$.
            \begin{itemize}
                \item
                The surjectivity of $G \to H$ implies the surjectivity of $V_0 \to W_0$.
                After replacing $x$ by another representative modulo $V_0$ we may thus assume that $x$ maps to $0$ in $W$.

                \item
                We have $\ker(W^* \to W) \subseteq I H^* \subseteq j(V_0)$:
                The first inclusion is clear.
                For the second one suppose we are given elements $\lambda \in I$ and $x \in H^*$.
                Then we can choose a preimage $y \in F$ of $\lambda$ and the element $u(1 \otimes y) x - u(x) (1 \otimes \lambda) \in V_0$ will be a preimage of $\lambda x$.

                Consequently, after replacing $x$ again we may assume that already $j(x) = 0$.

                \item
                By the exactness of $0 \to S \otimes_R U \to V \to W^* \to 0$ we now find a preimage of $x$ in $S \otimes_R U$.
                The image of this element in $C \otimes_R U/U_0$ will then be a preimage of $x \in V/V_0$. \qedhere
            \end{itemize}
        \end{itemize}
    \end{proof}

    \begin{lemma}[{\cite[Sections 2.2.1, 2.3.2 and 2.3.2]{lichtenbaum-schlessinger}, \cite[Exercises 3.7 and 3.8]{hartshorne}}] \label{lem:base-change}
        Let $A \to B$ and $A \to A'$ be maps in $\catrings$, set $B' \coloneqq A' \otimes_A B$ and suppose that either $B$ or $A'$ is flat over $A$.
        Then the natural map
        \[
            B' \otimes_B \LS_{B/A} \to \LS_{B'/A'}
        \]
        is an isomorphism in $\cathom_{\bullet}(B')$.

        Consequently we have
        \[
            T^i(B/A, M') \cong T^i(B'/A', M')
        \]
        in $\catmods(B)$ for all $M' \in \catmods(B')$ and $i = 0, 1, 2$.
        If moreover $A$ is Noetherian, $A \to B$ is of finite type and we are in the case that $A \to A'$ is flat then we even have
        \[
            B' \otimes_B T^i(B/A, M) \cong T^i(B'/A', B' \otimes_B M)
        \]
        in $\catmods(B')$ for all $M \in \catmods(B)$ and $i = 0, 1, 2$.
    \end{lemma}

    \begin{proof}
        Let $\calE \in \catalgs^+$ be projecting to $(A \to B)$ and write $\calE = (A, 0 \to U/U_0 \to F/U_0 \to R \to B \to 0)$ as in \Cref{def:algplus}.
        Then we set $R' \coloneqq A' \otimes_A R$, $I' \coloneqq R' \otimes_R I$, $F' \coloneqq R' \otimes_R F$ and $U' \coloneqq R' \otimes_R U$.

        The flatness assumption then implies that the sequences
        \[
            0 \to I' \to R' \to B' \to 0
        \]
        and
        \[
            0 \to U' \to F' \to I' \to 0
        \]
        are exact.
        Thus we obtain an object $\calE' \coloneqq (A', 0 \to U'/U'_0 \to F'/U'_0 \to R' \to B' \to 0) \in \catalgs^+$ projecting to $(A' \to B')$ and a natural map $\calE \to \calE'$ projecting to $(A \to B) \to (A' \to B')$.

        Using that $\Omega_{R'/B'} \cong R' \otimes_R \Omega_{R/B}$ we now immediately see that the natural map
        \[
            B' \otimes_B \LStild(\calE) \to \LStild(\calE')
        \]
        is an isomorphism in $\catch_{\bullet}(B')$.
        This in particular proves the first part of the lemma.

        The second part follows formally from this, using the following:
        Given that $A$ is Noetherian and $A \to B$ is of finite type then one can choose $\calE \in \catalgs^+$ in a way so that $\LStild(\calE)$ is a complex of finitely generated $B$-modules.
        This is needed because for finitely presented $B$-modules $N$ the natural map
        \[
            B' \otimes_B \Hom_B(N, M) \to \Hom_{B'}(B' \otimes_B N, B' \otimes_B M)
        \]
        is an isomorphism.
    \end{proof}

    \begin{lemma}[{\cite[Proposition 2.2.3 and Section 2.3.4]{lichtenbaum-schlessinger}, \cite[Exercise 3.5]{hartshorne}}] \label{lem:localization}
        Let $(A \to B) \in \catalgs$ and let $g \in B$.
        Then the natural map
        \[
            \LS_{B/A}[g^{-1}] \to \LS_{B[g^{-1}]/A}
        \]
        is an isomorphism in $\cathom_{\bullet}(B[g^{-1}])$.

        Consequently, if $A$ is Noetherian and $A \to B$ is of finite type, we obtain
        \[
            T^i(B/A, M)[g^{-1}] \cong T^i(B[g^{-1}]/A, M[g^{-1}])
        \]
        in $\catmods(B[g^{-1}])$ for all $M \in \catmods(B)$ and $i = 0, 1, 2$.
    \end{lemma}

    \begin{proof}
        See reference.
    \end{proof}

    \begin{remark}
        Using \Cref{lem:localization} it is possible to define functors
        \[
            T^i(X/S, \blank) \colon \catqcoh(X) \to \catqcoh(X)
        \]
        for any locally Noetherian scheme $S$ and $X/S$ locally of finite type.
    \end{remark}


    \begin{lemma}[{\cite[Propositions 3.6, 3.7, 3.8 and Corollary 3.9]{hartshorne}}] \label{lem:basic-props}
        Let $(A \to B) \in \catalgs$.
        \begin{itemize}
            \item
            Let $R \to B$ be a surjection from a polynomial $A$-algebra $R$ with kernel $I$.
            Then we have a canonical isomorphism
            \[
                \tau_{\leq 2} \LS_{B/A} \cong (0 \to I/I^2 \to B \otimes_R \Omega_{R/A} \to 0)
            \]
            where the differential is given by $\lambda \mapsto 1 \otimes d\lambda$.

            \item
            If $B$ is the quotient of a polynomial $A$-algebra by a regular sequence (or more generally an $H_1$-regular sequence in the sense of \cite[Tag 062E]{stacks-project}) then we have in fact $\LS_{B/A} \cong \tau_{\leq 2} \LS_{B/A}$ (also note that in this case $I/I^2$ is freely generated by the regular sequence).
        \end{itemize}
        We obtain the following consequences.
        \begin{itemize}
            \item
            We have a canonical isomorphism $H_0(\LS_{B/A}) \cong \Omega_{B/A}$.
            Thus $T^0(B/A, M)$ identifies with the $B$-module $\Der_A(B, M)$ of $A$-linear derivations $B \to M$.

            \item
            If $B$ is a polynomial $A$-algebra then we in fact have $\LS_{B/A} \cong \Omega_{B/A}[0]$.

            \item
            If $A \to B$ is surjective with kernel $I$ then we have a canonical isomorphism $H_1(\LS_{B/A}) \cong I/I^2$.

            \item
            If $B$ is the quotient of $A$ by an ideal $I$ generated by an $H_1$-regular sequence then we in fact have $\LS_{B/A} \cong I/I^2[1]$.
        \end{itemize}
    \end{lemma}

    \begin{proof}
        Given $R \to B$ as in the first part we can choose a free $R$-module $F$ surjecting onto $I$ and obtain $\calE \in \catalgs^+$ projecting to $(A \to B)$.
        Then we have a short exact sequence
        \[
            0 \to E_2 \to E_1 \to I \to 0
        \]
        that gives an isomorphism $\coker(E_2 \to B \otimes_R E_1) \cong I/I^2$.
        This proves the first part.

        For the second part choose $\calE = (A, 0 \to U/U_0 \to F/U_0 \to R \to B \to 0) \in \catalgs^+$ projecting to $(A \to B)$ such that the kernel $I$ of $R \to B$ is generated by a regular sequence $\lambda_1, \dotsc, \lambda_r$ and $F = R^r$ such that the map $F \to I$ is given by $e_i \mapsto \lambda_i$.
        Then $U/U_0$ is precisely the first homology of the Koszul complex $K_{\bullet}(\lambda_1, \dotsc, \lambda_k)$ (see \cite[Tag 0621]{stacks-project}) and thus vanishes by assumption.
    \end{proof}

    \begin{example}[{\cite[Exercise 3.3]{hartshorne}}]
        Let us compute the Lichtenbaum-Schlessinger complex in an example that isn't handled by \Cref{lem:basic-props}.
        Namely consider any ring $A$ and $B = A[x, y]/(x^2, xy, y^2)$.
        \begin{itemize}
            \item
            We choose $R = A[x, y]$ so that $I = (x^2, xy, y^2)$.

            \item
            We choose $F = R^3$ and $F \to I$ given by $e_1 \mapsto x^2$, $e_2 \mapsto xy$ and $e_3 \mapsto y^2$.
            
            \item
            The kernel $U = \ker(F \to I)$ is then freely generated by $f_1 = y e_1 - x e_2$ and $f_2 = y e_2 - x e_3$.

            \item
            The $R$-submodule $U_0 \subseteq U$ is generated by the elements
            \begin{align*}
                xy e_1 - x^2 e_2 &= x f_1, \\
                y^2 e_2 - xy e_3 &= y f_2, \\
                y^2 e_1 - x^2 e_3 &= y f_1 + x f_2.
            \end{align*}
        \end{itemize}
        The cotangent complex with respect to this presentation is thus given by
        \[
            0 \to B^2/(x e_1, y e_2, y e_1 + x e_2) \xrightarrow{\begin{psmallmatrix} y & 0 \\ -x & y \\ 0 & -x \end{psmallmatrix}} B^3 \xrightarrow{\begin{psmallmatrix} 2x & y & 0 \\ 0 & x & 2y \end{psmallmatrix}} B^2 \to 0.
        \]
        Note that it would have been sufficient to make this computation for $A = \ZZ$ and then apply \Cref{lem:base-change} to deduce it for general $A$.
    \end{example}

    \printbibliography
\end{document}